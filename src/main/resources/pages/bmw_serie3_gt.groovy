import geb.Browser
import geb.navigator.Navigator
import geb.waiting.WaitTimeoutException

import java.text.SimpleDateFormat

def results = [:] as Map<String, Map>

Browser.drive {
    go "http://www.bmwpremiumselection.es/"

    def waitSomeTime = { time = 250 ->
        Thread.sleep(time)
    }

    def serie = waitFor(message: "Serie 3 input") { $("#search_serie_3") }
    serie.click()

    def model = waitFor(message: "Gran turismo input") { $("#search_body_gran-turismo") }
    waitFor(message: "Gran turismo input is displayed") { model.displayed }
    model.click()

    def search = waitFor(message: "Search button") { $("#submit_search") }
    waitFor(message: "Search button is displayed") { search.displayed }
    search.click()

    def extractNumber = { String value ->
        if (!value) {
            return null
        }
        def matcher = value =~ /([\d|\\.]+)/
        if (matcher.find()) {
            return Integer.valueOf(matcher.group(1).replace(/./, ""))
        }
        return null
    }

    def extractDate = { String value ->
        if (!value) {
            return null
        }
        SimpleDateFormat df = new SimpleDateFormat("MMMM yyyy", new Locale("es"))
        return df.parse(value).getTime()
    }

    def extractText = { Navigator element ->
        if (!element) {
            return null
        }
        return element.getAttribute("textContent").trim()
    }

    def nextPage = true
    int page = 1
    while (nextPage) {
        log.info "Page ${page++}"
        waitSomeTime(1)

        def cars = waitFor(message: "Cars list") { $("article.row") }
        cars.each { car ->
            def anchor = (car.find("div").first().find("a")[0]).@href
            if (!results[anchor]) {
                def name = extractText(car.find("span", itemprop: "name"))
                def price = extractNumber(extractText(car.find("span", itemprop: "price")[1]))
                def originalPrice = extractNumber(extractText(car.find("em")))
                def registration = extractDate(extractText(car.find("div.description").find("li")[0]).replace("Matriculación: ", ""))
                def km = extractNumber(extractText(car.find("span.km")).replace(" km /", ""))
                results[anchor] = [
                    id           : anchor,
                    model        : name,
                    price        : price,
                    originalPrice: originalPrice,
                    discount     : 100 - (int) (price * 100F / originalPrice),
                    km           : km,
                    registration : registration,
                    warranty     : null,
                    location     : null
                ]
            }
        }
        try {
            def next = waitFor(message: "Next button") { $("span.next") }
            go next.find("a").@href as String
        } catch (WaitTimeoutException e) {
            nextPage = false
        }
    }

    results.each { entry ->
        log.info "Page ${entry.getKey()}"
        waitSomeTime(1)

        go entry.getKey()

        def warranty = null
        def monthWarranty = $("li", text: contains("meses de garantía"))
        if (monthWarranty.size() > 0) {
            warranty = extractNumber(extractText(monthWarranty))
        } else {
            def yearWarranty = $("strong", text: contains("años de garantía"))
            if (yearWarranty.size() > 0) {
                warranty = extractNumber(extractText(yearWarranty)) * 12
            }
        }
        def location = extractText($("div#location").find("div")[0].find("p")[0])

        entry.value.warranty = warranty
        entry.value.location = location
    }
}

return new LinkedList<>(results.values())
