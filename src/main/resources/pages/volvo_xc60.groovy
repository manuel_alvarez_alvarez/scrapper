import es.manuel.cs.dto.Column
import es.manuel.cs.dto.Result
import geb.Browser
import geb.navigator.Navigator
import geb.waiting.WaitTimeoutException

import java.text.SimpleDateFormat

def results = [:] as Map<String, Map>

Browser.drive {
    go "http://selekt.volvocars.es/"

    def waitSomeTime = { time = 250 ->
        Thread.sleep(time)
    }

    def xc60 = waitFor { $("span", text: "XC60") }
    waitFor { xc60.displayed }
    xc60.parent().click()

    def zipCode = waitFor {
        $("input", name: "ctl00\$ctl00\$ctl00\$cphcontent\$cphbodycontent\$searchoptionsctrl\$zipCode")
    }
    zipCode.value("33203")

    def distance = waitFor {
        $("select", name: "ctl00\$ctl00\$ctl00\$cphcontent\$cphbodycontent\$searchoptionsctrl\$proximityDistance")
    }
    distance.value("2000")

    def search = waitFor {
        $("input", name: "ctl00\$ctl00\$ctl00\$cphcontent\$cphbodycontent\$searchoptionsctrl\$submitButton")
    }
    search.click()

    def extractNumber = { String value ->
        if (!value) {
            return null
        }
        def matcher = value =~ /([\d|\\.]+)/
        if (matcher.find()) {
            return Integer.valueOf(matcher.group(1).replace(/./, ""))
        }
        return null
    }

    def extractDate = { String value ->
        if (!value) {
            return null
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy", new Locale("es"))
        return df.parse(value).getTime()
    }

    def extractText = { Navigator element ->
        if (!element) {
            return null
        }
        return element.getAttribute("textContent").trim()
    }

    def nextPage = true
    int page = 1
    while (nextPage) {
        log.info "Page ${page}"
        waitSomeTime(1)

        try {
            def cars = waitFor { $("ul.cars") }
            def navigation = waitFor { $("ul.navi") }
            navigation = navigation[0]
            cars.find("article").each { car ->
                def anchor = car.find("a")[0].@href as String
                anchor = anchor.substring(0, anchor.indexOf("/?searchOptions"))
                if (!results[anchor]) {
                    def info = car.find("div.vehicleinfo")
                    def nameAndDate = extractText(info.find("h4")[0].find("a"))
                    def nameAndDateMatcher = nameAndDate =~ /^([\d]{4})?\s?(.+)/
                    def name = null, registration = null
                    if (nameAndDateMatcher.find()) {
                        name = nameAndDateMatcher.group(2)
                        registration = extractDate(nameAndDateMatcher.group(1))
                    }
                    def price = extractNumber(extractText(car.find("p.price")))
                    def km = extractNumber(extractText(car.find("nobr")))
                    results[anchor] = [
                        id           : anchor,
                        model        : name,
                        price        : price,
                        km           : km,
                        registration : registration,
                        location     : null
                    ]
                }
            }

            page++
            def next = navigation.find("a", text: Integer.toString(page))
            if (next.size() == 0) {
                nextPage = false
            } else {
                next.click()
            }
        } catch (WaitTimeoutException e) {
            nextPage = false
        }
    }

    results.each { entry ->
        log.info "Page ${entry.getKey()}"
        waitSomeTime(1)

        go entry.getKey()

        def location = extractText($("div.addr").find("p")[0])
        entry.value.location = location
    }
}

return new LinkedList<>(results.values())
