import geb.Browser
import geb.navigator.Navigator
import geb.waiting.WaitTimeoutException
import org.openqa.selenium.JavascriptExecutor

import java.text.SimpleDateFormat

def results = [:] as Map<String, Map>

Browser.drive {
    go "http://suchen.mobile.de/fahrzeuge/search.html?lang=en"

    def waitSomeTime = { time = 250 ->
        Thread.sleep(time)
    }

    def keywords = waitFor(message: "Make input") { $("select", id: "selectMake1-ds") }
    keywords.value("3500")
    waitSomeTime()

    def brand = waitFor(message: "Model input") { $("select", id: "selectModel1-ds") }
    brand.value("76")

    def model = waitFor(message: "Description input") { $("input", id: "modelDescription1-ds") }
    model.value("Gran turismo")

    def country = waitFor(message: "Country input") { $("select", id: "ambitCountry-ds") }
    country.value("DE")

    def minRegistry = waitFor(message: "First registration date input") { $("input", id: "minFirstRegistrationDate") }
    minRegistry.value("2010-01-01")

    def maxMileage = waitFor(message: "Max mileage input") { $("input", id: "maxMileage") }
    maxMileage.value("80000")

    def maxPrice = waitFor(message: "Max price input") { $("input", id: "maxPrice") }
    maxPrice.value("35000")

    def withImages = waitFor(message: "With pictures input") { $("input", id: "withImage-true-ds") }
    (driver as JavascriptExecutor).executeScript("arguments[0].click();", withImages.firstElement())

    def searchButton = waitFor(message: "Search input") { $("button", id: "dsp-search-btn") }
    (driver as JavascriptExecutor).executeScript("arguments[0].click();", searchButton.firstElement())


    def extractNumber = { String value ->
        if (!value) {
            return null
        }
        def matcher = value =~ /([\d|\\,]+)/
        if (matcher.find()) {
            return Integer.valueOf(matcher.group(1).replace(/,/, ""))
        }
        return null
    }

    def extractDecimal = { String value ->
        if (!value) {
            return null
        }
        def matcher = value =~ /([\\.|\d|\\,]+)/
        if (matcher.find()) {
            return Double.valueOf(matcher.group(1).replace(/,/, ""))
        }
        return null
    }

    def extractDate = { String value ->
        if (!value) {
            return null
        }
        SimpleDateFormat df = new SimpleDateFormat("MM/yyyy", new Locale("es"))
        return df.parse(value).getTime()
    }

    def extractText = { Navigator element ->
        if (!element) {
            return null
        }
        return element.getAttribute("textContent").trim()
    }

    def nextPage = true
    int page = 1
    while (nextPage) {
        log.info "Page ${page++}"
        waitSomeTime(1)

        def extractCars = { String selector ->
            try {
                def cars = waitFor(message: "Cars list $selector") { $(selector) }
                cars.each { car ->
                    def anchor = car.find("a")[0].@href as String
                    if (anchor.indexOf("&") >= 0) {
                        anchor = anchor.substring(0, anchor.indexOf("&"))
                    }
                    def park = car.find("div.parking-block")
                    def name = park.attr("data-park-title")
                    def price = extractNumber(park.attr("data-park-price-amount"))

                    def km = null, registration = null, accidentFree = null, location = null, rate = null, reviews = null
                    def data = car.find("div.rbt-regMilPow")
                    if (data.size() > 0) {
                        data = data[0]
                        def children = data.parent().find("div.u-margin-bottom-9")
                        location = extractText(children[0])
                        def text = data.text()
                        if (!text.contains("New car")) {
                            def registrationEnd = text.indexOf(",") + 1
                            def registrationText = (text.substring(0, registrationEnd))
                            if (!text.contains("Pre-Registration")) {
                                registration = extractDate(registrationText.substring(3))
                            }
                            def kmEnd = text.indexOf("km,", registrationEnd)
                            km = extractNumber(text.substring(registrationEnd, kmEnd))
                        }
                        accidentFree = extractText(children[1]).contains("Accident-free") ? "Accident-free" : ""
                    }

                    def ratings = car.find(".mde-rating-s")
                    if (ratings.size() > 0) {
                        rate = extractDecimal(ratings.attr("data-score"))
                        reviews = extractNumber(ratings.attr("data-count"))
                    }

                    results[anchor] = [
                        id          : anchor,
                        model       : name,
                        price       : price,
                        km          : km,
                        registration: registration,
                        accidentFree: accidentFree,
                        location    : location,
                        rate        : rate,
                        reviews     : reviews
                    ]
                }
            } catch (WaitTimeoutException e) {
                // eat it
            }
        }

        extractCars("div.cBox-body--resultitem")

        try {
            def next = waitFor("Next page link") { $("a.rbt-page-forward") }
            next.click();
        } catch (WaitTimeoutException e) {
            nextPage = false
        }
    }
}

return new LinkedList<>(results.values())
