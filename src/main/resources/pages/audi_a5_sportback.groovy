import geb.Browser
import geb.navigator.Navigator
import geb.waiting.WaitTimeoutException
import org.openqa.selenium.JavascriptExecutor

import java.text.SimpleDateFormat

def results = [:] as Map<String, Map>

Browser.drive {
    go "http://audiselectionplus.es/buscador"

    def waitSomeTime = { time = 250 ->
        Thread.sleep(time)
    }

    def a5 = waitFor(message: "A5 selector") { $("a", href: "/vehiculos-de-ocasion_A5") }
    waitFor(message: "A5 selector is displayed") { a5.displayed }
    a5.click()

    def a5Sportback = waitFor(message: "Sportback selector") { $("a", href: "/vehiculos-de-ocasion_A5/A5_Sportback") }
    waitFor(message: "Sportback selector is displayed") { a5Sportback.displayed }
    a5Sportback.click()

    def extractNumber = { String value ->
        if (!value) {
            return null
        }
        def matcher = value =~ /([\d|\\.]+)/
        if (matcher.find()) {
            return Integer.valueOf(matcher.group(1).replace(/./, ""))
        }
        return null
    }

    def extractDate = { String value ->
        if (!value) {
            return null
        }
        SimpleDateFormat df = new SimpleDateFormat("MM/yyyy", new Locale("es"))
        return df.parse(value).getTime()
    }

    def extractText = { Navigator element ->
        if (!element) {
            return null
        }
        return element.getAttribute("textContent").trim()
    }

    def nextPage = true
    int page = 1
    while (nextPage) {
        log.info "Page ${page++}"
        waitSomeTime(1)
        try {
            def loading = waitFor(message: "Loading blocker") { $("#loading_blanket") }
            waitFor(message: "Loading blocker is gone") { !loading.displayed }
            Thread.sleep(500)
            def next = waitFor(message: "Next page button") { $("#mas-resultados") }
            waitFor(message: "Next page is displayed") { next.displayed }
            (driver as JavascriptExecutor).executeScript("arguments[0].click();", next.firstElement())
        } catch (WaitTimeoutException e) {
            nextPage = false
        }
    }

    $("div.anuncio").each { car ->
        def anchorElement = car.find("a", itemprop: "url")[0]
        def anchor = anchorElement.@href
        if (!results[anchor]) {

            log.info "Page ${anchor}"
            waitSomeTime(1)

            def name = extractText(anchorElement)
            def price = extractNumber(car.find("input", name: "precio").value()?.toString())
            def originalPrice = extractNumber(extractText(car.find("li", itemprop: "price")))
            def km = extractNumber(extractText(car.find("span", text: "Kilómetros:").parent()))
            def registration = extractDate(extractText(car.find("span", text: "Matriculación:").parent()).replace("Matriculación:", ""))
            def warranty = extractNumber(extractText(car.find("em", text: contains("Garantía"))))
            def location = extractText(car.find("div.anuncio-datos").find("li")[4]).replace("Provincia: ", "")
            results[anchor] = [
                id           : anchor,
                model        : name,
                price        : price,
                originalPrice: originalPrice,
                discount     : 100 - (int) (price * 100F / originalPrice),
                km           : km,
                registration : registration,
                warranty     : warranty,
                location     : location
            ]
        }
    }
}

return new LinkedList<>(results.values())
