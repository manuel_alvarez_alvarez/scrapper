import geb.Browser
import geb.navigator.Navigator
import geb.waiting.WaitTimeoutException
import org.openqa.selenium.JavascriptExecutor

import java.text.SimpleDateFormat

def results = [:] as Map<String, Map>

Browser.drive {
    go "http://www.coches.net/ocasion/"

    def waitSomeTime = { time = 250 ->
        Thread.sleep(time)
    }

    def keywords = waitFor(message: "Keywords input") { $("input", id: "txt_keyWords") }
    keywords.value("sportback")

    def brand = waitFor(message: "Brand input") { $("select", id: "ddl_Make") }
    brand.value("4")
    waitSomeTime()

    def model = waitFor(message: "Model input") { $("select", id: "ddl_Model") }
    model.value("611")
    waitSomeTime()

    def extractNumber = { String value ->
        if (!value) {
            return null
        }
        def matcher = value =~ /([\d|\\.]+)/
        if (matcher.find()) {
            return Integer.valueOf(matcher.group(1).replace(/./, ""))
        }
        return null
    }

    def extractDate = { String value ->
        if (!value) {
            return null
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy", new Locale("es"))
        return df.parse(value).getTime()
    }

    def extractText = { Navigator element ->
        if (!element) {
            return null
        }
        return element.getAttribute("textContent").trim()
    }

    def nextPage = true
    int page = 1
    while (nextPage) {
        log.info "Page ${page++}"
        waitSomeTime(1)

        def cars = waitFor(message: "Cars list") { $("article.mt-Card") }
        cars.each { car ->

            (driver as JavascriptExecutor).executeScript("arguments[0].scrollIntoView();", car.firstElement())
            waitSomeTime()

            def anchor = (car.find("a.mt-CardAd-link")[0]).@href
            if (!results[anchor]) {
                def name = extractText(car.find("h2.mt-CardAd-title").find(".mt-CardAd-titleHiglight"))
                def price = extractNumber(extractText(car.find("div.mt-CardAd-price").find(".mt-CardAd-titleHiglight")))
                def registration = extractDate(extractText(car.find("ul.mt-CardAd-attributesList").find("li")[2]))
                def km = extractNumber(extractText(car.find("ul.mt-CardAd-attributesList").find("li")[3]).replace(" km", ""))
                def location = extractText(car.find("ul.mt-CardAd-attributesList").find("li")[0])
                results[anchor] = [
                    id           : anchor,
                    model        : name,
                    price        : price,
                    originalPrice: null,
                    discount     : null,
                    km           : km,
                    registration : registration,
                    warranty     : null,
                    location     : location
                ]
            }
        }
        try {
            def next = waitFor("Next page link") { $("a.mt-Pagination-link--next") }
            if (next.hasClass("is--disabled")) {
                nextPage = false
            } else {
                go next.@href as String
            }
        } catch (WaitTimeoutException e) {
            nextPage = false
        }
    }

    results.each { entry ->
        log.info "Page ${entry.getKey()}"
        waitSomeTime(1)

        go entry.getKey()

        def originalPrice = null
        def discount = null
        def warranty = null

        def data = $("div", id: "div_datosvehiculo")
        def warrantyText = data.find("b", text: "Garantía:");
        if (warrantyText.size() > 0) {
            warranty = extractNumber(extractText(warrantyText.parent()))
        }

        def fix = $("div", id: "ctrl_caixaficha")
        if (fix.size() > 0) {

            (driver as JavascriptExecutor).executeScript("arguments[0].scrollIntoView();", fix.firstElement())

            try {
                def priceContainer = waitFor(message: "Original price paragraph") { fix.find("b") }
                if (priceContainer.size() > 0) {
                    originalPrice = extractNumber(extractText(priceContainer[0]))
                    discount = 100 - (int) (entry.value.price * 100F / originalPrice)
                }
            } catch (WaitTimeoutException e) {

            }
        }

        entry.value.originalPrice = originalPrice
        entry.value.discount = discount
        entry.value.warranty = warranty

    }
}

return new LinkedList<>(results.values())
