#!/usr/bin/env bash
export DISPLAY=:1.5
#sleep 1
Xvfb :1 -screen 5 1366x768x24 &
sleep 1
firefox --display=:1.5 &
sleep 1
#google-chrome &
java -Djava.security.egd=file:/dev/./urandom -Dwdm.targetPath=/repository -Dspring.profiles.active=prod -XX:+DisableAttachMechanism -Dcom.sun.management.jmxremote.port=4041 -Dcom.sun.management.jmxremote.rmi.port=4041 -Djava.rmi.server.hostname=127.0.0.1 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -jar app.war
