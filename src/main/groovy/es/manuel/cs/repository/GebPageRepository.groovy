package es.manuel.cs.repository

import es.manuel.cs.domain.GebPage
import es.manuel.cs.domain.ResultRow
import groovy.transform.CompileStatic
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Repository to store geb pages.
 */
@Repository
@CompileStatic
interface GebPageRepository extends JpaRepository<GebPage, UUID> {

}
