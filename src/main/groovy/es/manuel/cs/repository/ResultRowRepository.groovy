package es.manuel.cs.repository

import es.manuel.cs.domain.GebPage
import es.manuel.cs.domain.ResultRow
import groovy.transform.CompileStatic
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Repository for the result rows.
 */
@Repository
@CompileStatic
interface ResultRowRepository extends JpaRepository<ResultRow, UUID> {

    List<ResultRow> findByPage(GebPage page)

    void deleteByPage(GebPage page)

}
