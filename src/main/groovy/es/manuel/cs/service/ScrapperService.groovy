package es.manuel.cs.service

import groovy.transform.CompileStatic
import groovy.util.logging.Commons
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.security.Security

/**
 * Base class for the scrapper service.
 */
@CompileStatic
@Service
@Commons
class ScrapperService {

    @PostConstruct
    public void setup() {
        Security.insertProviderAt(new BouncyCastleProvider(), 2); // set BC as default
    }

    final List<Map<String, Object>> scrap(String script) {
        def binding = new Binding(log: log)
        def shell = new GroovyShell(Thread.currentThread().contextClassLoader, binding)
        def result = shell.evaluate(script)
        return result as List<Map<String, Object>>
    }
}
