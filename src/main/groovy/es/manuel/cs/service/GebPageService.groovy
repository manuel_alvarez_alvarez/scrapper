package es.manuel.cs.service

import com.fasterxml.jackson.databind.ObjectMapper
import es.manuel.cs.domain.GebPage
import es.manuel.cs.domain.ResultColumn
import es.manuel.cs.domain.ResultRow
import es.manuel.cs.domain.Status
import es.manuel.cs.dto.*
import es.manuel.cs.executor.NotifiableThreadPoolTaskExecutor
import es.manuel.cs.executor.ThreadPoolTaskExecutorListener
import es.manuel.cs.repository.GebPageRepository
import es.manuel.cs.repository.ResultRowRepository
import groovy.transform.CompileStatic
import groovy.util.logging.Commons
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationContext
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.support.TransactionCallback
import org.springframework.transaction.support.TransactionTemplate
import org.springframework.util.ReflectionUtils

import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import java.util.concurrent.Callable
import java.util.concurrent.Future
import java.util.concurrent.FutureTask

/**
 * Service for the pages
 */
@Service
@CompileStatic
@Commons
class GebPageService implements ThreadPoolTaskExecutorListener {

    private static final Long TIMEOUT_CHECKER_RATE = 1800000L // 30 minutes
    private static final Integer TIMEOUT = 600000 // 10 minutes

    @Autowired
    private GebPageRepository gebPageRepository

    @Autowired
    private ResultRowRepository resultRowRepository

    @Autowired
    private ScrapperService scrapperService

    @Autowired
    private ObjectMapper objectMapper

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private PlatformTransactionManager transactionManager

    @Autowired
    @Qualifier("pageRefreshExecutor")
    private NotifiableThreadPoolTaskExecutor executor

    private final Map<UUID, RefreshTask> tasks = [:]
    private TransactionTemplate transactionTemplate

    @PostConstruct
    public void setup() {
        transactionTemplate = new TransactionTemplate(transactionManager)
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT)
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW)
        transactionTemplate.setReadOnly(false)

        executor.addListener(this)
    }

    @PreDestroy
    public void cleanup() {
        executor.removeListener(this)
    }

    private RefreshCallable getCallable(FutureTask task) {
        def field = ReflectionUtils.findField(FutureTask, "callable")
        ReflectionUtils.makeAccessible(field)
        return ReflectionUtils.getField(field, task) as RefreshCallable
    }

    @Override
    void beforeExecute(Thread t, Runnable r) {
        def callable = getCallable(r as FutureTask)
        this.tasks[callable.page] = new RefreshTask(
            page: callable.page,
            start: callable.start,
            shuttingDown: false,
            future: r as Future
        )
    }

    @Override
    void afterExecute(Runnable r, Throwable t) {
        def task = this.tasks.values().find { it.future == r }
        this.tasks.remove(task.page)
    }

    @Transactional(readOnly = true)
    public List<Entry> list() {
        return gebPageRepository.findAll().collect {
            def task = tasks[it.uuid]
            return new Entry(
                page: it,
                task: task
            )
        }
    }

    @Transactional(readOnly = true)
    public GebPage find(UUID id) {
        return gebPageRepository.findOne(id);
    }

    @Transactional
    public GebPage create(GebPage page) {
        page.setUuid(UUID.randomUUID())
        page.status = Status.NEW
        return gebPageRepository.save(page)
    }

    @Transactional
    public void update(GebPage page) {
        gebPageRepository.save(page)
    }

    @Transactional
    public void delete(UUID id) {
        GebPage page = find(id)
        resultRowRepository.deleteByPage(page)
        gebPageRepository.delete(page)
    }

    public void refresh(final UUID id) {
        refresh(Collections.singletonList(id));
    }

    public void stop(final UUID id) {
        def task = this.tasks[id];
        if (task) {
            task.future.cancel(true)
            task.shuttingDown = true
        }
    }

    public void refreshAll() {
        refresh(gebPageRepository.findAll().collect { it.uuid });
    }

    public void refresh(final List<UUID> list) {
        Date start = new Date()
        GebPageService proxy = applicationContext.getBean(GebPageService.class);
        list.each { UUID page ->
            if (!tasks.containsKey(page)) {
                executor.submit(new RefreshCallable(page, start, proxy))
            }
        }
    }

    @Scheduled(fixedRate = GebPageService.@TIMEOUT_CHECKER_RATE)
    public void stopTasks() {
        def list = new LinkedList<>(this.tasks.keySet()) as List<UUID>
        list.each {
            def task = this.tasks[it]
            if (task) {
                if (new Date().getTime() - task.getStart().getTime() > TIMEOUT) {
                    stop(task.page)
                }
            }
        }
    }

    @Scheduled(cron = '${scrapper.scheduled.cron}')
    public void updateAll() {
        refresh(gebPageRepository.findAll().collect { it.uuid })
    }

    public GebPage evaluate(UUID uuid) {
        Date now = new Date()
        GebPage page = null
        try {
            withTransaction {
                page = gebPageRepository.findOne(uuid)
                page.status = Status.REFRESHING
                gebPageRepository.save(page)
            }
            withTransaction {
                page = gebPageRepository.findOne(uuid)
                def toSave = [] as Set<ResultRow>
                def existing = resultRowRepository.findByPage(page).collectEntries {
                    [(it.href): it]
                } as Map<String, ResultRow>
                def results = scrapperService.scrap(page.page)
                results.each { Map<String, Object> values ->
                    def href = values.id as String
                    def row = existing.remove(href)
                    if (!row) {
                        row = new ResultRow(
                            uuid: UUID.randomUUID(),
                            page: page,
                            href: href,
                            since: now,
                            columns: [] as Set<ResultColumn>
                        )
                    }
                    def columnsMap = row.columns.collectEntries({ [(it.name): it] }) as Map<String, ResultColumn>
                    values.each { name, value ->
                        def column = columnsMap[name] as ResultColumn
                        if (!column) {
                            column = new ResultColumn(
                                uuid: UUID.randomUUID(),
                                name: name?.toString(),
                                row: row
                            )
                            row.columns << column
                        }
                        column.value = value
                    }
                    row.columns.removeAll { columnsMap.containsKey(it.name) }
                    toSave << row
                }

                page.lastUpdate = now
                gebPageRepository.save(page)
                resultRowRepository.save(toSave)
                resultRowRepository.delete(existing.values())
            }
            withTransaction {
                page = gebPageRepository.findOne(uuid)
                page.status = Status.OK
                gebPageRepository.save(page)
            }
        } catch (Throwable e1) {
            log.error("Error refreshing page ${page?.title}", e1)
            try {
                withTransaction {
                    page = gebPageRepository.findOne(uuid)
                    page.status = Status.FAILED
                    gebPageRepository.save(page)
                }
            } catch (Throwable e2) {
                // don't shallow the original stack
                log.error("Error setting status to failed for page ${page?.title}", e2)
            }
            throw e1;
        }
        return page;
    }

    @Transactional(readOnly = true)
    public Result export(GebPage page) {
        def configuration = parseConfiguration(page)
        def rows = resultRowRepository.findByPage(page)
        def list = rows.collect { ResultRow row ->
            def columns = row.columns.collectEntries { [(it.name): it] } as Map<String, ResultColumn>
            def list = configuration.getColumns().collect {
                return it.type.parseValue(columns[it.name]?.value)
            }
            list << row.since
            return list
        }
        configuration.columns << new Column(name: "since", label: "Fecha visto", type: Column.Type.DATE, order: Column.Order.DESC)
        return new Result(configuration: configuration, list: list)
    }

    @Transactional(readOnly = true)
    public Workbook exportAsExcel(UUID uuid) {
        def page = gebPageRepository.findOne(uuid)
        def workbook = new XSSFWorkbook()
        addExcelSheet(workbook, page)
        return workbook
    }

    @Transactional(readOnly = true)
    public Workbook exportAllAsExcel() {
        def pages = gebPageRepository.findAll()
        def workbook = new XSSFWorkbook()
        pages.each {
            addExcelSheet(workbook, it)
        }
        return workbook
    }

    public void addExcelSheet(Workbook workbook, GebPage page) {
        def result = export(page)
        def styles = Column.Type.values().collectEntries {
            [(it), it.initializeWorkbook(workbook)]
        } as Map<Column.Type, CellStyle>

        def headerStyle = workbook.createCellStyle()
        headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND)
        Font font = workbook.createFont();
        font.setColor(IndexedColors.WHITE.getIndex());
        headerStyle.setFont(font);

        def sheet = workbook.createSheet(page.title)
        def row = sheet.createRow(0)
        result.configuration.columns.eachWithIndex { column, index ->
            def cell = row.createCell((index as Integer).intValue(), Cell.CELL_TYPE_STRING)
            cell.setCellValue(column.label)
            cell.setCellStyle(headerStyle)
        }
        result.list.eachWithIndex { List entry, i ->
            def dataRow = sheet.createRow(i + 1)
            result.configuration.columns.eachWithIndex { column, index ->
                column.type.createCell(
                    styles[column.type],
                    workbook,
                    sheet,
                    dataRow,
                    (index as Integer).intValue(),
                    entry.get((index as Integer).intValue())
                )
            }
        }
        sheet.setAutoFilter(
            new CellRangeAddress(
                0,
                result.list.size(),
                0,
                result.configuration.columns.size() - 1
            )
        );

        result.configuration.columns.eachWithIndex { Column entry, i -> sheet.autoSizeColumn(i) }
    }

    private Configuration parseConfiguration(GebPage page) {
        def configuration = objectMapper.readerFor(Configuration.class).readValue(page.config) as Configuration
        configuration.columns.sort { it.index }
        return configuration
    }

    private <E> E withTransaction(Closure<E> runnable) {
        return (E) transactionTemplate.execute(new TransactionCallback() {
            @Override
            Object doInTransaction(TransactionStatus status) {
                return runnable()
            }
        })
    }

    private class RefreshCallable implements Callable<GebPage> {
        private final UUID page
        private final Date start
        private final GebPageService proxy

        RefreshCallable(UUID page, Date start, GebPageService proxy) {
            this.page = page
            this.start = start
            this.proxy = proxy
        }

        @Override
        GebPage call() throws Exception {
            return proxy.evaluate(page)
        }
    }


}
