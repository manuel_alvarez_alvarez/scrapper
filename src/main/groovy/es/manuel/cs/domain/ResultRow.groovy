package es.manuel.cs.domain

import groovy.transform.EqualsAndHashCode
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.Type

import javax.persistence.*

/**
 * Result row after scrapping.
 */
@Entity
@EqualsAndHashCode(includes = "uuid")
class ResultRow {

    @Id
    @Type(type = "uuid-char")
    UUID uuid

    @Lob
    String href

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "page_uuid", referencedColumnName = "uuid")
    GebPage page

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "row")
    @Fetch(FetchMode.SUBSELECT)
    Set<ResultColumn> columns

    @Temporal(TemporalType.TIMESTAMP)
    Date since
}
