package es.manuel.cs.domain

/**
 * Enum with the status of the geb pages.
 */
enum Status {

    NEW,
    OK,
    REFRESHING,
    FAILED
}
