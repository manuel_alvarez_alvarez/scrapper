package es.manuel.cs.domain

import groovy.transform.EqualsAndHashCode
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.Type

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

/**
 * Data of a column
 */
@Entity
@EqualsAndHashCode(includes = "uuid")
class ResultColumn {

    @Id
    @Type(type = "uuid-char")
    UUID uuid

    @ManyToOne
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "row_uuid", referencedColumnName = "uuid")
    ResultRow row

    String name

    @Lob
    String value
}
