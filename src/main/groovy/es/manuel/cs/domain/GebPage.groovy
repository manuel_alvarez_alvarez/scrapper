package es.manuel.cs.domain

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import org.hibernate.annotations.Type

import javax.persistence.*

/**
 * Saved GEB page that is able to scrap the code from a car page.
 */
@Entity
@CompileStatic
@EqualsAndHashCode(includes = "uuid")
class GebPage {

    @Id
    @Type(type = "uuid-char")
    UUID uuid

    String title

    @Lob
    String page

    @Lob
    String config

    @Temporal(TemporalType.TIMESTAMP)
    Date lastUpdate

    @Enumerated(EnumType.STRING)
    Status status
}
