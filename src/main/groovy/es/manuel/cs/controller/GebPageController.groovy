package es.manuel.cs.controller

import es.manuel.cs.domain.GebPage
import es.manuel.cs.dto.Entry
import es.manuel.cs.dto.Result
import es.manuel.cs.service.GebPageService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

import javax.servlet.http.HttpServletResponse

/**
 * Controller for geb pages.
 */
@RequestMapping(value = "/pages")
@Controller
@CompileStatic
class GebPageController {

    private static
    final String XLSX_MEDIA_TYPE_VALUE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"

    @Autowired
    private GebPageService gebPageService

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Entry> list() {
        return gebPageService.list()
    }

    @RequestMapping(
        value = "/{uuid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseBody
    public Result get(@PathVariable("uuid") UUID uuid) {
        def page = gebPageService.find(uuid);
        return gebPageService.export(page)
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public GebPage create(@RequestBody GebPage page) {
        return gebPageService.create(page)
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody GebPage page) {
        gebPageService.update(page)
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("uuid") UUID id) {
        gebPageService.delete(id)
    }

    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    @ResponseBody
    public void refreshAll() {
        gebPageService.refreshAll()
    }

    @RequestMapping(value = "/{uuid}/refresh", method = RequestMethod.GET)
    @ResponseBody
    public void refresh(@PathVariable("uuid") UUID uuid) {
        gebPageService.refresh(uuid)
    }

    @RequestMapping(value = "/{uuid}/stop", method = RequestMethod.PUT)
    @ResponseBody
    public void stop(@PathVariable("uuid") UUID uuid) {
        gebPageService.stop(uuid)
    }

    @RequestMapping(
        value = "/{uuid}/export",
        method = RequestMethod.GET,
        produces = GebPageController.@XLSX_MEDIA_TYPE_VALUE
    )
    public void exportAsExcel(@PathVariable("uuid") UUID uuid, HttpServletResponse response) {
        response.setContentType(XLSX_MEDIA_TYPE_VALUE);
        response.setHeader(
            HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=\"export.xlsx\"");

        def workbook = gebPageService.exportAsExcel(uuid)
        def os = response.outputStream
        workbook.write(new OutputStream() {
            @Override
            void write(int b) throws IOException {
                os.write(b)
            }
        })
        os.flush()
    }

    @RequestMapping(
        value = "/export",
        method = RequestMethod.GET,
        produces = GebPageController.@XLSX_MEDIA_TYPE_VALUE
    )
    public void exportAllAsExcel(HttpServletResponse response) {
        response.setContentType(XLSX_MEDIA_TYPE_VALUE);
        response.setHeader(
            HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=\"export.xlsx\"");

        def workbook = gebPageService.exportAllAsExcel()
        def os = response.outputStream
        workbook.write(new OutputStream() {
            @Override
            void write(int b) throws IOException {
                os.write(b)
            }
        })
        os.flush()
    }
}
