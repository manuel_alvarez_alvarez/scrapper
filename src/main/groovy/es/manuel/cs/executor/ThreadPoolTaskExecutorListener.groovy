package es.manuel.cs.executor

/**
 * Listeners for the threads
 */
interface ThreadPoolTaskExecutorListener {

    void beforeExecute(Thread t, Runnable r)
    void afterExecute(Runnable r, Throwable t)

}
