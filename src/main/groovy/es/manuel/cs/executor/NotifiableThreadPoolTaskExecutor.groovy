package es.manuel.cs.executor

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.util.ReflectionUtils

import java.util.concurrent.*

/**
 * Executor to whom we can add listeners
 */
class NotifiableThreadPoolTaskExecutor extends ThreadPoolTaskExecutor {

    private <E> E get(String property, Class<E> type) {
        def field = ReflectionUtils.findField(ThreadPoolTaskExecutor, property)
        ReflectionUtils.makeAccessible(field)
        return (E) ReflectionUtils.getField(field, this)
    }

    private void set(String property, def value) {
        def field = ReflectionUtils.findField(ThreadPoolTaskExecutor, property)
        ReflectionUtils.makeAccessible(field)
        ReflectionUtils.setField(field, this, value)
    }

    @Override
    protected ExecutorService initializeExecutor(
        ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {
        BlockingQueue<Runnable> queue = createQueue(get("queueCapacity", Integer))
        ThreadPoolExecutor executor = new NotifiableThreadPoolExecutor(
            this.corePoolSize, this.maxPoolSize, this.keepAliveSeconds, TimeUnit.SECONDS,
            queue, threadFactory, rejectedExecutionHandler)

        if (get("allowCoreThreadTimeOut", Boolean)) {
            executor.allowCoreThreadTimeOut(true)
        }
        set("threadPoolExecutor", executor)
        return executor;
    }

    @Override
    public NotifiableThreadPoolExecutor getThreadPoolExecutor() throws IllegalStateException {
        return super.getThreadPoolExecutor() as NotifiableThreadPoolExecutor
    }

    public void addListener(ThreadPoolTaskExecutorListener listener) {
        this.getThreadPoolExecutor().addListener(listener)
    }

    public void removeListener(ThreadPoolTaskExecutorListener listener) {
        this.getThreadPoolExecutor().removeListener(listener)
    }

    private static class NotifiableThreadPoolExecutor extends ThreadPoolExecutor {

        private final List<ThreadPoolTaskExecutorListener> listeners = []

        NotifiableThreadPoolExecutor(int corePoolSize, int maxPoolSize, long keepAliveSeconds, TimeUnit unit, BlockingQueue<Runnable> queue, ThreadFactory factory, RejectedExecutionHandler reject) {
            super(corePoolSize, maxPoolSize, keepAliveSeconds, unit, queue, factory, reject)
        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            listeners.each { it.afterExecute(r, t) }
        }

        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            listeners.each { it.beforeExecute(t, r) }
        }

        void addListener(ThreadPoolTaskExecutorListener listener) {
            this.listeners.add(listener)
        }

        void removeListener(ThreadPoolTaskExecutorListener listener) {
            this.listeners.remove(listener)
        }
    }
}
