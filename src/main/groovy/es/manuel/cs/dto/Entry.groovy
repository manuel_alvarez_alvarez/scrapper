package es.manuel.cs.dto

import es.manuel.cs.domain.GebPage

/**
 * Entry for the list
 */
class Entry {

    GebPage page
    RefreshTask task
}
