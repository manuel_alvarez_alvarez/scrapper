package es.manuel.cs.dto

import java.util.concurrent.Future

/**
 * Task for a refresh
 */
class RefreshTask {
    UUID page
    Date start
    boolean shuttingDown
    Future<?> future
}
