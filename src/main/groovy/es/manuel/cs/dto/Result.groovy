package es.manuel.cs.dto

import groovy.transform.CompileStatic

/**
 * Result of the evaluation.
 */
@CompileStatic
class Result {

    List<List> list
    Configuration configuration
}
