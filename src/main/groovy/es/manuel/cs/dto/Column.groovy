package es.manuel.cs.dto

import groovy.transform.CompileStatic
import org.apache.poi.common.usermodel.Hyperlink
import org.apache.poi.ss.usermodel.*

/**
 * Meta model for the columns
 */
@CompileStatic
class Column {

    Integer index
    String name
    String label
    Type type
    Order order

    enum Type {
        NUMBER{
            @Override
            Cell createCell(CellStyle style, Workbook workbook, Sheet sheet, Row row, int index, def value) {
                def cell = row.createCell(index, Cell.CELL_TYPE_NUMERIC)
                if (value != null) {
                    cell.setCellValue((value as Number).doubleValue())
                }
                return cell
            }

            @Override
            Object parseValue(String value) {
                if (!value) {
                    return null
                }
                return new BigDecimal(value)
            }
        },
        STRING{
            @Override
            Cell createCell(CellStyle style, Workbook workbook, Sheet sheet, Row row, int index, def value) {
                def cell = row.createCell(index, Cell.CELL_TYPE_STRING)
                if (value != null) {
                    cell.setCellValue(value as String)
                }
                return cell
            }

            @Override
            Object parseValue(String value) {
                return value
            }
        },
        DATE{
            @Override
            Cell createCell(CellStyle style, Workbook workbook, Sheet sheet, Row row, int index, def value) {
                def cell = row.createCell(index, Cell.CELL_TYPE_NUMERIC)
                cell.cellStyle = style
                if (value != null) {
                    cell.setCellValue(value as Date)
                }
                return cell
            }

            @Override
            Object parseValue(String value) {
                if (!value) {
                    return null
                }
                return new Date(Long.valueOf(value))
            }

            @Override
            CellStyle initializeWorkbook(Workbook workbook) {
                def style = workbook.createCellStyle()
                def helper = workbook.getCreationHelper()
                style.setDataFormat(
                    helper.createDataFormat().getFormat("dd-MM-yyyy")
                )
                return style
            }
        },
        LINK{
            @Override
            Cell createCell(CellStyle style, Workbook workbook, Sheet sheet, Row row, int index, def value) {
                def cell = row.createCell(index, Cell.CELL_TYPE_STRING)
                if (value != null) {
                    try {
                        Hyperlink link = workbook.creationHelper.createHyperlink(Hyperlink.LINK_URL)
                        link.address = value
                        cell.hyperlink = link
                        cell.cellStyle = style
                        cell.setCellValue("Link")
                    } catch (Throwable e) {
                        cell.setCellValue(value as String)
                    }
                }
                return cell
            }

            @Override
            Object parseValue(String value) {
                return value
            }

            @Override
            CellStyle initializeWorkbook(Workbook workbook) {
                def style = workbook.createCellStyle()
                Font font = workbook.createFont();
                font.setUnderline(Font.U_SINGLE);
                font.setColor(IndexedColors.BLUE.getIndex());
                style.setFont(font);
                return style
            }
        }

        abstract Cell createCell(CellStyle style, Workbook workbook, Sheet sheet, Row row, int index, def value)

        abstract Object parseValue(String value)

        CellStyle initializeWorkbook(Workbook workbook) {
            return null
        }
    }

    enum Order {
        ASC,
        DESC
    }
}
