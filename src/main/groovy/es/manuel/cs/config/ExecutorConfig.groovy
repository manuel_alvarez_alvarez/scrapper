package es.manuel.cs.config

import es.manuel.cs.executor.NotifiableThreadPoolTaskExecutor
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jmx.export.MBeanExporter
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

/**
 * Configuration for the executors.
 */
@Configuration
class ExecutorConfig {

    @Value('${scrapper.scheduled.corePoolSize}')
    private int corePoolSize

    @Value('${scrapper.scheduled.maxPoolSize}')
    private int maxPoolSize

    @Bean
    NotifiableThreadPoolTaskExecutor pageRefreshExecutor() {
        return new NotifiableThreadPoolTaskExecutor(
            corePoolSize: corePoolSize,
            maxPoolSize: maxPoolSize
        )
    }

    @Bean
    protected MBeanExporter pageRefreshExecutorExporter(ThreadPoolTaskExecutor pageRefreshExecutor) {
        MBeanExporter exporter = new MBeanExporter();
        Map<String,Object> beans = new HashMap<>();
        beans.put("org.springframework.boot:type=executor,name=pageRefreshExecutor", pageRefreshExecutor);
        exporter.setBeans(beans);
        return exporter;
    }
}
