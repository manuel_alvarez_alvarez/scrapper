import io.github.bonigarcia.wdm.ChromeDriverManager
import io.github.bonigarcia.wdm.PhantomJsDriverManager
import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeDriverService
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxBinary
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities

driver = { return firefox() }
cache = false
cacheDriverPerThread = true


static def htmlUnit() {
    return prepareDriver(new HtmlUnitDriver(capabilities()))
}

static def phantomJs() {
    PhantomJsDriverManager.getInstance().setup()
    def capabilities = capabilities()
    capabilities.setCapability('phantomjs.cli.args', ["--ignore-ssl-errors=yes", "--load-images=no"] as String[])
    def service = PhantomJSDriverService.createDefaultService(capabilities)
    return prepareDriver(new PhantomJSDriver(service, capabilities))
}

static def firefox() {
    def firefoxBinary = new FirefoxBinary()
    firefoxBinary.setEnvironmentProperty("DISPLAY", ":1.5");
    def firefoxProfile = new FirefoxProfile()
    firefoxProfile.setPreference("permissions.default.image", 2)
    firefoxProfile.setPreference("browser.migration.version", 9999)
    return prepareDriver(new FirefoxDriver(firefoxBinary, firefoxProfile, capabilities()))
}

static def chrome() {
    ChromeDriverManager.getInstance().setup()
    def options = new ChromeOptions()
    def imageBlocker = new File("/Block-image_v1.1.crx")
    if (imageBlocker.exists()) {
        options.addExtensions(imageBlocker)
    }
    def capabilities = capabilities()
    capabilities.setCapability(ChromeOptions.CAPABILITY, options)
    def service = new ChromeDriverService.Builder().usingAnyFreePort().withEnvironment([
        "DISPLAY": ":1.5"
    ]).build()
    return prepareDriver(new ChromeDriver(service, capabilities))
}

static def capabilities() {
    return new DesiredCapabilities([
        'javascriptEnabled': true
    ])
}

static def prepareDriver(WebDriver driver) {
    driver.manage().window().setSize(new Dimension(1366, 768))
    return driver
}

