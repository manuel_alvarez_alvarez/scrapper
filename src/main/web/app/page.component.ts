import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Page} from "./model";
import {PageService} from "./page.service";
import {EditorDirective} from "./editor.directive";

@Component({
    selector: 'my-page',
    templateUrl: 'app/page.component.html',
    directives: [EditorDirective]
})
export class PageComponent {
    @Input() page:Page;
    error:any;
    @Output() close = new EventEmitter();

    constructor(private pageService:PageService) {
    }

    save() {
        this.pageService
            .save(this.page)
            .then(page => {
                this.page = page;
                this.goBack(page);
            })
            .catch(error => this.error = error);
    }

    goBack(savedPage:Page = null) {
        this.close.emit(savedPage);
    }
}
