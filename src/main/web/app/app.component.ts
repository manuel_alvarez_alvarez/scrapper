import {Component, OnInit, OnDestroy, ElementRef, Input} from "@angular/core";
import {RefreshTask, Entry, Page, Result} from "./model";
import {PageService} from "./page.service";
import {PageComponent} from "./page.component";
import {DatatableComponent} from "./datatable.component";
import {LoaderComponent} from "./loader.component";
import {EditorDirective} from "./editor.directive";

declare var $:any;
declare var moment:any;


@Component({
    selector: 'my-scrapper',
    templateUrl: 'app/app.component.html',
    providers: [PageService],
    directives: [PageComponent, DatatableComponent, LoaderComponent, EditorDirective]
})
export class AppComponent implements OnInit, OnDestroy {
    @Input() schedule:number = 60000;
    pages:Entry[] = [];
    lastLoaded:string = '';
    error:string[] = [];
    info:string[] = [];
    errorResult:string[] = [];
    page:Page = null;
    interval:any = null;

    result:Result = null;

    constructor(private pageService:PageService, private elementRef:ElementRef) {
    }

    getPages() {
        this.pageService
            .getPages()
            .then(pages => {
                this.pages = pages;
                this.lastLoaded = `List loaded at ${moment(new Date()).format("DD/MM/YYYY HH:mm:ss")}`;
            })
            .catch(error => this.error.push(error));
    }

    ngOnInit() {
        this.getPages();
        this.interval = setInterval(this.getPages.bind(this), this.schedule);
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

    addPage() {
        this.cleanUp();
        this.showModal(() => this.page = new Page());
    }

    editPage(page:Page) {
        this.cleanUp();
        this.showModal(() => this.page = page);
    }

    deletePage(page:Page) {
        this.cleanUp();
        this.pageService
            .deletePage(page)
            .then(() => {
                this.getPages();
                this.info.push("The page has been deleted succesfully");
            })
            .catch(error => this.error.push(error));
    }

    view(page:Page) {
        this.cleanUp();
        this.showResultsModal(() => {
            this.pageService
                .view(page)
                .then(result => this.result = result)
                .catch(error => this.errorResult.push(error));
        });
    }

    refresh(page:Page) {
        this.cleanUp();
        this.pageService
            .refresh(page)
            .then(() => {
                this.getPages();
                this.info.push("The refresh has been triggered wait a few moments until it is complete and refresh your page");
            })
            .catch(error => this.error.push(error));
    }
    
    stopTask(task:RefreshTask) {
        this.cleanUp();
        this.pageService
            .stopTask(task)
            .then(() => {
                this.getPages();
                this.info.push("The task has been stopped");
            })
            .catch(error => this.error.push(error));
    }

    refreshAll() {
        this.cleanUp();
        if (this.pages.length > 0) {
            this.pageService
                .refreshAll()
                .then(() => {
                    this.getPages();
                    this.info.push("The refresh has been triggered wait a few moments until it is complete and refresh your page");
                })
                .catch(error => this.error.push(error));
        } else {
            this.error.push("There are no pages tp refresh");
        }
    }

    exportAsExcel(page:Page) {
        this.cleanUp();
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style.display = "none";
        a.href = this.pageService.exportAsExcel(page);
        a.click();
    }

    exportAsExcelAll() {
        this.cleanUp();
        if (this.pages.length > 0) {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style.display = "none";
            a.href = this.pageService.exportAsExcelAll();
            a.click();
        } else {
            this.error.push("There are no pages to export as excell");
        }
    }

    close(savedPage:Page) {
        this.hideModal(() => {
            this.cleanUp();
            if (savedPage) {
                this.getPages();
                this.info.push("The page has been saved successfully");
            }
        });
    }

    private cleanUp() {
        this.info.splice(0, this.info.length);
        this.error.splice(0, this.error.length);
        this.errorResult.splice(0, this.errorResult.length);
        this.result = null;
        this.page = null;
    }

    private showModal(action) {
        $("#page-modal").one("shown.bs.modal", action).modal("show");
    }

    private hideModal(action) {
        $("#page-modal").one("hidden.bs.modal", action).modal("hide");
    }

    private showResultsModal(action) {
        $("#result-modal").one("shown.bs.modal", action).modal("show");
    }

}
