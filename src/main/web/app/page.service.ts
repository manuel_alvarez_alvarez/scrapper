import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {RefreshTask, Entry, Page, Result} from "./model";

declare var $:any;

@Injectable()
export class PageService {

    private pagesUrl = 'pages';

    constructor(private http:Http) {
    }

    getPages():Promise<Entry[]> {
        return this.http.get(this.pagesUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    save(page:Page):Promise<Page> {
        if (page.uuid) {
            return this.put(page);
        }
        return this.post(page);
    }

    deletePage(page:Page):Promise<any> {
        return this.del(page);
    }

    view(page:Page):Promise<Result> {
        let url = `${this.pagesUrl}/${page.uuid}`;
        return this.http
            .get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    refresh(page:Page):Promise<any> {
        let url = `${this.pagesUrl}/${page.uuid}/refresh`;
        return this.http
            .get(url)
            .toPromise()
            .catch(this.handleError);
    }
    
    stopTask(task: RefreshTask): Promise<any> {
        let url = `${this.pagesUrl}/${task.page}/stop`;
        return this.http
            .put(url, "")
            .toPromise()
            .catch(this.handleError);
    }

    refreshAll():Promise<any> {
        let url = `${this.pagesUrl}/refresh`;
        return this.http
            .get(url)
            .toPromise()
            .catch(this.handleError);
    }

    exportAsExcel(page:Page):string {
        return `${this.pagesUrl}/${page.uuid}/export`;
    }

    exportAsExcelAll():string {
        return `${this.pagesUrl}/export`;
    }

    private post(page:Page):Promise<Page> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http
            .post(this.pagesUrl, JSON.stringify(page), {headers: headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // Update existing Hero
    private put(page:Page) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let url = `${this.pagesUrl}/${page.uuid}`;
        return this.http
            .put(url, JSON.stringify(page), {headers: headers})
            .toPromise()
            .then(() => page)
            .catch(this.handleError);
    }

    private del(page:Page) {
        let url = `${this.pagesUrl}/${page.uuid}`;
        return this.http
            .delete(url)
            .toPromise()
            .then(() => true)
            .catch(this.handleError);
    }

    private handleError(error:any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
