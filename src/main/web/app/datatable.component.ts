import {Component, ElementRef, Inject, Input} from "@angular/core";
import {Result} from "./model";

declare var $:any;
declare var moment:any;

@Component({
    selector: 'my-table',
    templateUrl: 'app/datatable.component.html'
})
export class DatatableComponent {

    @Input() result:Result;

    constructor(@Inject(ElementRef) private elementRef:ElementRef) {
    }

    ngAfterViewInit() {
        var order = [];
        this.result.configuration.columns.forEach((column, index) => {
            if (column.hasOwnProperty("order") && column.order !== null) {
                order.push([index, column.order.toLowerCase()])
            }
        });
        var params = {
            data: this.result.list,
            columns: this.result.configuration.columns.map((value) => {
                return {
                    title: value.label,
                    render: this.renderer(value)
                }
            }),
            order
        };
        $(this.elementRef.nativeElement).find("table").DataTable(params);
    }

    private renderer(column) {
        var render;
        switch (column.type) {
            case "NUMBER":
                render = this.renderNumber;
                break;
            case "DATE":
                render = this.renderDate;
                break;
            case "LINK":
                render = this.renderLink;
                break;
            default:
                render = this.renderString;
                break;
        }
        return render(column);
    }

    private renderString(column) {
        return function (text) {
            return text;
        }
    }

    private renderNumber(column) {
        return function (number) {
            return number;
        }
    }

    private renderDate(column) {
        return function (date) {
            if (date == null) {
                return null
            }
            return moment(parseInt(date)).format("DD/MM/YYYY")
        }
    }

    private renderLink(column) {
        return function (link) {
            return `<a target="_blank" href="${link}">${column.label}</a>`
        }
    }
}
