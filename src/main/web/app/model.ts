export class Entry {
    page: Page;
    task: RefreshTask;
}

export class Page {
    uuid:string;
    title:string;
    page:string;
    config: string;
    lastUpdate: Date;
    status: string;
}

export class RefreshTask {
    start: Date;
    page: string;
    shuttingDown: boolean;
}

export class Result {
    list:any[];
    configuration:Configuration;
}

export class Configuration {
    columns:Column[]
}

export class Column {
    label:string;
    type:string;
    order:string;
}
