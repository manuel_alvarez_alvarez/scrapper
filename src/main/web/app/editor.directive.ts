import {Directive, ElementRef} from "@angular/core";
import {NgModel} from "@angular/common";

declare var CodeMirror:any;

@Directive({
    selector: '[ngModel][editor]'
})
export class EditorDirective {

    editor:any;

    constructor(private el:ElementRef, private model:NgModel) {
    }

    ngAfterViewInit() {
        var element = this.el.nativeElement;
        var mode = element.dataset.editorMode;
        this.editor = CodeMirror.fromTextArea(
            element,
            {
                lineNumbers: true,
                matchBrackets: true,
                mode: mode
            }
        );
        var self = this;
        this.editor.on("blur", function (instance) {
            instance.save();
            self.model.valueAccessor.writeValue(element.value);
            self.model.viewToModelUpdate(element.value);
        });
    }

}
