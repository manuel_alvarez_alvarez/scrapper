'use strict';

// require
var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var ts = require('gulp-typescript');
var vinylPaths = require('vinyl-paths');
var tsProject = ts.createProject('tsconfig.json', { typescript: require('typescript') });

// vars
var staticDir = '../../../build/web-resources/static/';

// lib copy
gulp.task('libcopy', function () {
    // clean dest using sync
    del.sync([staticDir + 'node_modules/**'], { force: true });

    gulp.src(['./node_modules/@angular/**/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/@angular'));

    // copy @angular dependencies
    gulp.src(['./node_modules/core-js/client/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/core-js/client'));

    gulp.src(['./node_modules/zone.js/dist/*.js'])
        .pipe(gulp.dest(staticDir + 'node_modules/zone.js/dist'));

    gulp.src(['./node_modules/reflect-metadata/temp/*.js'])
        .pipe(gulp.dest(staticDir + 'node_modules/reflect-metadata/temp'));

    gulp.src(['./node_modules/systemjs/dist/*.js'])
        .pipe(gulp.dest(staticDir + 'node_modules/systemjs/dist'));

    gulp.src(['./node_modules/codemirror/lib/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/codemirror/lib'));

    gulp.src(['./node_modules/codemirror/mode/groovy/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/codemirror/mode/groovy'));

    gulp.src(['./node_modules/codemirror/mode/javascript/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/codemirror/mode/javascript'));

    gulp.src(['./node_modules/moment/*.js'])
        .pipe(gulp.dest(staticDir + 'node_modules/moment'));

    gulp.src(['./node_modules/bootstrap/dist/**/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/bootstrap/dist'));

    gulp.src(['./node_modules/bootstrap-material-design/dist/**/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/bootstrap-material-design/dist'));

    gulp.src(['./node_modules/jquery/dist/**/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/jquery/dist'));

    gulp.src(['./node_modules/datatables/media/**/*'])
        .pipe(gulp.dest(staticDir + 'node_modules/datatables/media'));
});

// html/config copy
gulp.task('htmlcopy', function () {
    // clean dest
    del([staticDir + 'index.html',
        staticDir + 'systemjs.config.js',
        staticDir + 'app/**/*.html'], { force: true });

    // copy index && systemjs config
    gulp.src(['./index.html', './systemjs.config.js'])
        .pipe(gulp.dest(staticDir));

    // copy angular templates
    gulp.src('./app/**/*.html')
        .pipe(gulp.dest(staticDir + 'app'));
});

// html watch
gulp.task('htmlw', function () {
    // watch index && systemjs config
    gulp.watch(['./index.html', './systemjs.config.js'], ['htmlcopy']);

    // watch angular templates
    gulp.watch('./app/**/*.html', ['htmlcopy']);
});

// sass compile
gulp.task('sass', function() {
    // clean dest
    del([staticDir + 'css/*'], {force: true});

    // compile sass and copy
    return gulp.src('./sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(staticDir + 'css'));
});

// sass watch compile
gulp.task('sassw', function() {
    gulp.watch('./sass/**/*.scss', ['sass']);
});

// typescript compile
gulp.task('tsc', function () {
    // clean src dest
    del([staticDir + 'app/**/*.js'], { force: true });

    // compile typescript
    var tsResult = tsProject.src().pipe(ts(tsProject));

    // copy
    var result = tsResult.js.pipe(gulp.dest(staticDir));

    // delete compiled
    gulp.src(["!./app/**/*.ts", "!./app/**/*.html", "./app/**/*"]).pipe(vinylPaths(del));

    return result;
});

// typescript watch compile
gulp.task('tscw', function () {
    gulp.watch(['./app/**/*.ts',
            './app/**/*.html'],
        ['htmlcopy', 'tsc']);
});

// build sass and ts, copy libs, copy html
gulp.task('build', ['htmlcopy', 'sass', 'tsc', 'libcopy']);

// watch sass, ts, and html
gulp.task('watch', ['build', 'sassw', 'htmlw', 'tscw']);

// default
gulp.task('default', ['build']);
